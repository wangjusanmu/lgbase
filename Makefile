## 二进制名
BINARY_NAME=logbase
## 入口位置
MAIN_PATH=./src/main.go

.PHONY: all
all: win linux

.PHONY: linux
linux:
	GOOS=linux GOARCH=amd64 go build $(RACE) -o ./bin/$(BINARY_NAME)-linux $(MAIN_PATH)

.PHONY: win
win:
	GOOS=windows GOARCH=amd64 go build $(RACE) -o ./bin/$(BINARY_NAME)-win.exe $(MAIN_PATH)

install:
	go mod tidy

build: win linux