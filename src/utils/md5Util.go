package utils

import (
	"crypto/md5"
	"fmt"
)

func GetMd5(data string) string {
	hash := md5.New()
	sum := hash.Sum([]byte(data))
	return fmt.Sprintf("%x", sum)
}
