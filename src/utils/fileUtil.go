package utils

import (
	"io/fs"
	"path/filepath"
	"strings"
)

func GetFilesByDir(dir string, suffix []string) ([]string, error) {
	files := make([]string, 0, 30)
	err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		for _, s := range suffix {
			if strings.HasSuffix(path, s) {
				files = append(files, path)
				break
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}
