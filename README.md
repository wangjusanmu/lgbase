# lgbase

#### 介绍
日志收集，通过Go编写简单监听中间件，推送向zinc

#### 安装教程

1. 下载发行版 [https://gitee.com/wangjusanmu/lgbase/releases](https://gitee.com/wangjusanmu/lgbase/releases)
2. 解压到服务器
3. 修改配置文件与脚本
   * conf/app.ini
     ```ini
     [log]
     ## 日志路径
     path : ./ ../logs
     ## 日志文件
     suffix : .log
     [zinc]
     ## zinc地址
     url : http://localhost:7777
     ## zinc账户
     username : admin
     ## zinc密码
     pwd : wjsmc
     ```
     
   * start.sh
   
     ```ini
     # zinc账户
     export ZINC_FIRST_ADMIN_USER=wjsmc
     # zinc密码
     export ZINC_FIRST_ADMIN_PASSWORD=wjsmc
     # zinc端口
     export ZINC_SERVER_PORT=7777
     ```

#### 源码编译

​	下载源码后，进入根目录执行

```shell
sudo yum install -y make
make
```

#### 使用说明

​	解压后start.sh即可,监视地址为自定义的http://localhost:7777

​	stop.sh即可关闭