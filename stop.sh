BIN_NAME=logbase
ZINC_NAME=zinc

pid_bin=`ps -ef|grep ${BIN_NAME}|grep -v grep|awk '{print $2}'`
if [ -z "${pid_bin}" ]; then
    echo ${BIN_NAME} no runing
else
    kill -9 ${pid_bin}
    echo ${pid_bin} kill
fi
pid_zinc=`ps -ef|grep ${ZINC_NAME}|grep -v grep|awk '{print $2}'`
if [ -z "${pid_zinc}" ]; then
 echo ${ZINC_NAME} no runing
else
   kill -9 ${pid_zinc}
   echo ${pid_zinc} kill
fi